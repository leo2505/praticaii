package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import br.com.empresa.pratica2.model.LancAvulso;

@Repository
public interface LancAvulsoRepository extends JpaRepository<LancAvulso, Integer> {
        @Query("select coalesce(sum(lan.valor),0)as total from LancAvulso lan "
                        + " inner join EventoFuncionario eve on(eve.idevento=lan.idevento and eve.idfuncionario=lan.idfuncionario) "
                        + " inner join Evento e on(e.idevento=lan.idevento) where lan.idfuncionario.idfuncionario=?1 "
                        + " and upper(lan.idevento.tipoevento)=upper(?2) and e.incideinss=1 and lan.idevento <>2 and lan.idevento <>3 and lan.idevento <>1"
                        + " and eve.mes=?3 and eve.ano=?4")
        public float valorLancAvulso(int idfuncionario, String tipo, int mes, int ano);

        @Query("select lan from LancAvulso lan "
                        + " inner join EventoFuncionario e on(e.idevento=lan.idevento and e.idfuncionario=lan.idfuncionario) "
                        + " inner join Evento eve on(eve.idevento=lan.idevento) "
                        + " where lan.idfuncionario.idfuncionario=?1 and e.mes=?2 and e.ano=?3 and eve.incideinss=0 "
                        + " and eve.idevento <>6 and eve.idevento <>8 "
                        + " and eve.idevento <>2 and eve.idevento <>3 and eve.idevento <>1")
        public List<LancAvulso> ListaLancAvulso(int idfuncionario, int mes, int ano);
}
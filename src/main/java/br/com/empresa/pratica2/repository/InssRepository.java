package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.Inss;

@Repository
public interface InssRepository extends JpaRepository<Inss, Integer> {
    @Query("select i from Inss i where valorinicial<=?1 and valorfinal>=?1")
    public Inss buscaPorSalario(double salario);
}
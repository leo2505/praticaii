package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.HorarioFuncionario;

@Repository
public interface HorarioFuncionarioRepository extends JpaRepository<HorarioFuncionario, Integer> {

    @Query("SELECT h "+
    " FROM HorarioFuncionario h join h.idfuncionario  "+
    " where h.idfuncionario.idfuncionario=?1 and h.mes=?2 and h.ano=?3")

    public HorarioFuncionario buscaPorIdFuncionario(int idfuncionario, int mes, int ano);
}
package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.Irrf;

@Repository
public interface IrrfRepository extends JpaRepository<Irrf, Integer> {
    @Query("select i from Irrf i where valorinicial<=?1 and valorfinal>=?1")
    public Irrf buscaPorSalario(double salario);
}
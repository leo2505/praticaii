package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.Dependente;

@Repository
public interface DependenteRepository extends JpaRepository<Dependente, Integer> {

    @Query("select count(*) from Dependente d where d.idfuncionario.idfuncionario=?1 and d.depir=true")
    public int CountDependentesFuncionario(int idfuncionario);
}
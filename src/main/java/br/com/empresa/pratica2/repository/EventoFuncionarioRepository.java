package br.com.empresa.pratica2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.EventoFuncionario;

@Repository
public interface EventoFuncionarioRepository extends JpaRepository<EventoFuncionario, Integer> {

    @Query("select e from EventoFuncionario e where e.idfuncionario.idfuncionario=?1 and e.mes=?2 and e.ano=?3")
    public List<EventoFuncionario> listaEventosFuncionario(int idfuncionario, int mes, int ano);

    @Query("select count(*) from EventoFuncionario e where e.idfuncionario.idfuncionario=?1 and e.mes=?2 and e.ano=?3 and e.idevento.idevento=?4")
    public int CountEventoFuncionario(int idfuncionario, int mes, int ano, int idevento);

    @Query("select e from EventoFuncionario e where e.idfuncionario.idfuncionario=?1 and e.mes=?2 and e.ano=?3 and e.idevento.idevento=?4")
    public EventoFuncionario EventoFuncionarioPorEvento(int idfuncionario, int mes, int ano, int idevento);
}
package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import br.com.empresa.pratica2.model.EventoFixo;

@Repository
public interface EventoFixoRepository extends JpaRepository<EventoFixo, Integer> {
    @Query("select coalesce(sum(ef.valor),0)as total from EventoFixo ef "
            + " join ef.idevento where ef.idfuncionario.idfuncionario=?1 and upper(ef.idevento.tipoevento)=upper(?4) "
            + " and ef.idevento.incideinss=1 and ef.idevento.idevento <>2 and ef.idevento.idevento <>3 "
            + " and ef.idevento.idevento <>1 and DATE_PART('MONTH', data_fim)>=?2 and DATE_PART('YEAR', data_fim)>=?3")
    public float valorEvento(int idfuncionario, int mes, int ano, String tipo);

    @Query("select ef from EventoFixo ef join ef.idevento where ef.idfuncionario.idfuncionario=?1 "
            + " and ef.idevento.incideinss=0 and ef.idevento.idevento <>2 and ef.idevento.idevento <>3 "
            + " and ef.idevento.idevento <>6 and ef.idevento.idevento <>8 "
            + " and ef.idevento.idevento <>1 and DATE_PART('MONTH', data_fim)>=?2 and DATE_PART('YEAR', data_fim)>=?3")
    public List<EventoFixo> ListaEventosFixo(int idfuncionario, int mes, int ano);
}
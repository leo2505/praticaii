package br.com.empresa.pratica2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.empresa.pratica2.model.Calculo;

@Repository
public interface CalculoRepository extends JpaRepository<Calculo, Integer> {
    @Query("select count(*) from Calculo d where d.idfuncionario.idfuncionario=?1 and d.fechado=1 and d.mes=?2 and d.ano=?3")
    public int CountCalculo(int idfuncionario, int mes, int ano);
}
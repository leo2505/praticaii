package br.com.empresa.pratica2.resources;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import javax.sql.DataSource;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import br.com.empresa.pratica2.model.Funcionario;
import br.com.empresa.pratica2.model.HistoricoCargo;
import br.com.empresa.pratica2.model.HistoricoSalario;
import br.com.empresa.pratica2.repository.FuncionarioRepository;
import br.com.empresa.pratica2.repository.HistoricoCargoRepository;
import br.com.empresa.pratica2.repository.HistoricoSalarioRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@RestController
@RequestMapping("funcionario")
public class FuncionarioResource {
    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private HistoricoCargoRepository historicoCargoRepository;

    @Autowired
    private HistoricoSalarioRepository historicoSalarioRepository;

    @Autowired
    private DataSource dataSource;

    @GetMapping("/report")
    public ResponseEntity<byte[]> relatorioFichaFuncional(@RequestParam("idfuncionario") int idfuncionario) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("idFuncionario", idfuncionario);

        byte[] reportBytes = null;

        try {
            InputStream io = this.getClass().getResourceAsStream("/reports/ficha_funcional_report.jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(io, parametros, dataSource.getConnection());

            reportBytes = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException | SQLException ex) {
            return ResponseEntity.noContent().build();
        }
        Funcionario funcionario = funcionarioRepository.findById(idfuncionario).get();

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=\"fichaFuncional_" + funcionario.getNomefuncionario() + ".pdf\"")
                .body(reportBytes);
    }

    @PostMapping
    public ResponseEntity<Funcionario> salvar(@Valid @RequestBody Funcionario funcionario) {
        Funcionario funcionarioSalvo = funcionarioRepository.save(funcionario);

        HistoricoCargo histCargo = new HistoricoCargo();
        histCargo.setDatainicial(new Date());
        histCargo.setIdcargo(funcionarioSalvo.getIdcargo());
        histCargo.setIdfuncionario(funcionarioSalvo);

        historicoCargoRepository.save(histCargo);

        HistoricoSalario histSalario = new HistoricoSalario();
        histSalario.setDatainicial(new Date());
        histSalario.setIdfuncionario(funcionarioSalvo);
        histSalario.setMotivoalteracao("Funcionário Novo");
        histSalario.setValorsalario(funcionarioSalvo.getSalariofuncionario());

        historicoSalarioRepository.save(histSalario);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(funcionarioSalvo.getIdfuncionario()).toUri();

        return ResponseEntity.created(location).body(funcionarioSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Funcionario> alterar(@PathVariable Integer id, @Valid @RequestBody Funcionario fun) {

        return funcionarioRepository.findById(id).map(record -> {
            if (record.getIdcargo() != fun.getIdcargo()) {
                HistoricoCargo histCargo = new HistoricoCargo();
                histCargo.setDatainicial(new Date());
                histCargo.setIdcargo(fun.getIdcargo());
                histCargo.setIdfuncionario(record);

                historicoCargoRepository.save(histCargo);
            }

            if (record.getSalariofuncionario() != fun.getSalariofuncionario()) {
                if (fun.getSalariofuncionario() > record.getSalariofuncionario()) {// aumento de salário
                    HistoricoSalario histSalario = new HistoricoSalario();
                    histSalario.setDatainicial(new Date());
                    histSalario.setIdfuncionario(record);
                    histSalario.setMotivoalteracao("Aumento do Salário");
                    histSalario.setValorsalario(fun.getSalariofuncionario());

                    historicoSalarioRepository.save(histSalario);
                } else {// dimiuição do salário
                    HistoricoSalario histSalario = new HistoricoSalario();
                    histSalario.setDatainicial(new Date());
                    histSalario.setIdfuncionario(record);
                    histSalario.setMotivoalteracao("Diminuição do Salário");
                    histSalario.setValorsalario(fun.getSalariofuncionario());

                    historicoSalarioRepository.save(histSalario);
                }
            }

            record.setAgenciabancofuncionario(fun.getAgenciabancofuncionario());
            record.setCnh1habilitacaofuncionario(fun.getCnh1habilitacaofuncionario());
            record.setCnhcategoriafuncionario(fun.getCnhcategoriafuncionario());
            record.setCnhexpedicaofuncionario(fun.getCnhexpedicaofuncionario());
            record.setCnhfuncionario(fun.getCnhfuncionario());
            record.setCnhvencimentofuncionario(fun.getCnhvencimentofuncionario());
            record.setCodbancofuncionario(fun.getCodbancofuncionario());
            record.setComplementoendereco(fun.getComplementoendereco());
            record.setContafgtsfuncionario(fun.getContafgtsfuncionario());
            record.setCpffuncionario(fun.getCpffuncionario());
            record.setCtpsexpedicaofuncionario(fun.getCtpsexpedicaofuncionario());
            record.setCtpsseriefuncionario(fun.getCtpsseriefuncionario());
            record.setDataadmissaofuncionario(fun.getDataadmissaofuncionario());
            record.setEmailfuncionario(fun.getEmailfuncionario());
            record.setEnderecofuncionario(fun.getEnderecofuncionario());
            record.setExpedicaorgfuncionario(fun.getExpedicaorgfuncionario());
            record.setIdcargo(fun.getIdcargo());
            record.setIdpessoa(fun.getIdpessoa());
            record.setIdsetor(fun.getIdsetor());
            record.setLogradourofuncionario(fun.getLogradourofuncionario());
            record.setNomefuncionario(fun.getNomefuncionario());
            record.setNomemaefuncionario(fun.getNomemaefuncionario());
            record.setNomepaifuncionario(fun.getNomepaifuncionario());
            record.setNumcontabancofuncionario(fun.getNumcontabancofuncionario());
            record.setOrgaorgfuncionario(fun.getOrgaorgfuncionario());
            record.setPisfuncionario(fun.getPisfuncionario());
            record.setRgfuncionario(fun.getRgfuncionario());
            record.setSalariofuncionario(fun.getSalariofuncionario());
            record.setTelefonefuncionario(fun.getTelefonefuncionario());
            record.setUfrgfuncionario(fun.getUfrgfuncionario());
            record.setIdhorario(fun.getIdhorario());

            Funcionario updated = funcionarioRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<Funcionario> findAll() {
        return funcionarioRepository.findAll();
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Funcionario> findByCodigo(@PathVariable Integer codigo) {
        try {
            Funcionario funcionario = funcionarioRepository.findById(codigo).get();

            return ResponseEntity.ok(funcionario);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        funcionarioRepository.deleteById(codigo);
    }
}
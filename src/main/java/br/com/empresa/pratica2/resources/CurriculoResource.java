package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Curriculo;
import br.com.empresa.pratica2.repository.CurriculoRepository;

@RestController
@RequestMapping("curriculo")
public class CurriculoResource {
    @Autowired
    private CurriculoRepository curriculoRepository;

    @GetMapping
    public List<Curriculo> findAll() {
        return curriculoRepository.findAll();
    }

    @GetMapping("/search")
    public List<Curriculo> pesquisaPorNomepessoaLike(@RequestParam("nomepessoa") String nomepessoa) {
        return curriculoRepository.pesquisaPorNomepessoa("%" + nomepessoa + "%");
    }

    @GetMapping("/search2")
    public List<Curriculo> pesquisaPorIdade(@RequestParam("idade") int idade) {
        return curriculoRepository.pesquisaPorIdade(idade);
    }

    @GetMapping("/search3")
    public List<Curriculo> pesquisaPorSexo(@RequestParam("sexo") int idgenero) {
        return curriculoRepository.pesquisaPorSexo(idgenero);
    }

    @GetMapping("/search4")
    public List<Curriculo> pesquisaPorEscolaridade(@RequestParam("escolaridade") int idescolaridade) {
        return curriculoRepository.pesquisaPorEscolaridade(idescolaridade);
    }

    @GetMapping("/search5")
    public List<Curriculo> pesquisaPorCidade(@RequestParam("cidade") String cidade) {
        return curriculoRepository.pesquisaPorCidade("%" + cidade + "%");
    }

    @PostMapping
    public ResponseEntity<Curriculo> salvar(@Valid @RequestBody Curriculo curriculo) {
        Curriculo curriculoSalvo = curriculoRepository.save(curriculo);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(curriculoSalvo.getIdcurriculo()).toUri();

        return ResponseEntity.created(location).body(curriculoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Curriculo> alterar(@PathVariable Integer id, @Valid @RequestBody Curriculo curriculo) {

        return curriculoRepository.findById(id).map(record -> {

            record.setExperienciascurriculo(curriculo.getExperienciascurriculo());
            record.setHabilidadescurriculo(curriculo.getHabilidadescurriculo());
            record.setIdpessoa(curriculo.getIdpessoa());
            record.setIdvaga(curriculo.getIdvaga());
            Curriculo updated = curriculoRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Curriculo> findByCodigo(@PathVariable Integer codigo) {
        try {
            Curriculo curriculo = curriculoRepository.findById(codigo).get();

            return ResponseEntity.ok(curriculo);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        curriculoRepository.deleteById(codigo);
    }
}
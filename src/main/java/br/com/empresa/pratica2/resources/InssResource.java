package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Inss;
import br.com.empresa.pratica2.repository.InssRepository;

@RestController
@RequestMapping("inss")
public class InssResource {
    @Autowired
    private InssRepository inssRepository;

    @GetMapping
    public List<Inss> findAll() {
        return inssRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Inss> salvar(@Valid @RequestBody Inss inss) {
        Inss inssSalvo = inssRepository.save(inss);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(inssSalvo.getIdinss()).toUri();

        return ResponseEntity.created(location).body(inssSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Inss> alterar(@PathVariable Integer id, @Valid @RequestBody Inss inss) {

        return inssRepository.findById(id).map(record -> {

            record.setAliquota(inss.getAliquota());
            record.setValidade(inss.getValidade());
            record.setValorfinal(inss.getValorfinal());
            record.setValorinicial(inss.getValorinicial());

            Inss updated = inssRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Inss> findByCodigo(@PathVariable Integer codigo) {
        try {
            Inss inss = inssRepository.findById(codigo).get();

            return ResponseEntity.ok(inss);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        inssRepository.deleteById(codigo);
    }
}
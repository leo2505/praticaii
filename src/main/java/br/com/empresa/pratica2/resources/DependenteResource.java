package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Dependente;
import br.com.empresa.pratica2.repository.DependenteRepository;

@RestController
@RequestMapping("dependente")
public class DependenteResource {
    @Autowired
    private DependenteRepository dependentesRepository;

    @GetMapping
    public List<Dependente> findAll() {
        return dependentesRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Dependente> salvar(@Valid @RequestBody Dependente dependentes) {
        Dependente dependentesSalvo = dependentesRepository.save(dependentes);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(dependentesSalvo.getIddependente()).toUri();

        return ResponseEntity.created(location).body(dependentesSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Dependente> alterar(@PathVariable Integer id, @Valid @RequestBody Dependente dependente) {

        return dependentesRepository.findById(id).map(record -> {

            record.setCpfdependente(dependente.getCpfdependente());
            record.setDatanascimentodependente(dependente.getDatanascimentodependente());
            record.setDepir(dependente.getDepir());
            record.setIdfuncionario(dependente.getIdfuncionario());
            record.setNomedependente(dependente.getNomedependente());
            Dependente updated = dependentesRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Dependente> findByCodigo(@PathVariable Integer codigo) {
        try {
            Dependente dependentes = dependentesRepository.findById(codigo).get();

            return ResponseEntity.ok(dependentes);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        dependentesRepository.deleteById(codigo);
    }
}
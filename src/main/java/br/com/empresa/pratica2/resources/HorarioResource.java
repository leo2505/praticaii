package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Horario;
import br.com.empresa.pratica2.repository.HorarioRepository;

@RestController
@RequestMapping("horario")
public class HorarioResource {

    @Autowired
    private HorarioRepository horarioRepository;

    @GetMapping
    public List<Horario> findAll() {
        return horarioRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Horario> salvar(@Valid @RequestBody Horario horario) {
        Horario horarioSalvo = horarioRepository.save(horario);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(horarioSalvo.getIdhorario()).toUri();

        return ResponseEntity.created(location).body(horarioSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Horario> alterar(@PathVariable Integer id, @Valid @RequestBody Horario hora) {

        return horarioRepository.findById(id).map(record -> {

            record.setDescricaohorario(hora.getDescricaohorario());
            record.setFimmanha(hora.getFimmanha());
            record.setFimtarde(hora.getFimtarde());
            record.setInimanha(hora.getInimanha());
            record.setInitarde(hora.getInitarde());

            Horario updated = horarioRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Horario> findByCodigo(@PathVariable Integer codigo) {
        try {
            Horario horario = horarioRepository.findById(codigo).get();

            return ResponseEntity.ok(horario);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        horarioRepository.deleteById(codigo);
    }
}
package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Tipovaga;
import br.com.empresa.pratica2.repository.TipovagaRepository;

@RestController
@RequestMapping("tipovaga")
public class TipovagaResource {
    @Autowired
    private TipovagaRepository tipovagaRepository;

    @GetMapping
    public List<Tipovaga> findAll() {
        return tipovagaRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Tipovaga> salvar(@Valid @RequestBody Tipovaga tipovaga) {
        Tipovaga tipovagaSalvo = tipovagaRepository.save(tipovaga);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(tipovagaSalvo.getIdtipovaga()).toUri();

        return ResponseEntity.created(location).body(tipovagaSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Tipovaga> alterar(@PathVariable Integer id, @Valid @RequestBody Tipovaga tip) {

        return tipovagaRepository.findById(id).map(record -> {

            record.setDescricaotipovaga(tip.getDescricaotipovaga());

            Tipovaga updated = tipovagaRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Tipovaga> findByCodigo(@PathVariable Integer codigo) {
        try {
            Tipovaga tipovaga = tipovagaRepository.findById(codigo).get();

            return ResponseEntity.ok(tipovaga);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        tipovagaRepository.deleteById(codigo);
    }
}
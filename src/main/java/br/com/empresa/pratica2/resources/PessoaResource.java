package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Pessoa;
import br.com.empresa.pratica2.repository.PessoaRepository;

@RestController
@RequestMapping("pessoa")
public class PessoaResource {
    @Autowired
    private PessoaRepository pessoaRepository;

    @GetMapping
    public List<Pessoa> findAll() {
        return pessoaRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Pessoa> salvar(@Valid @RequestBody Pessoa pessoa) {
        Pessoa pessoaSalvo = pessoaRepository.save(pessoa);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(pessoaSalvo.getIdpessoa()).toUri();

        return ResponseEntity.created(location).body(pessoaSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Pessoa> alterar(@PathVariable Integer id, @Valid @RequestBody Pessoa pes) {

        return pessoaRepository.findById(id).map(record -> {

            record.setContatopessoa(pes.getContatopessoa());
            record.setCpfpessoa(pes.getCpfpessoa());
            record.setDatanascimentopessoa(pes.getDatanascimentopessoa());
            record.setEmailpessoa(pes.getEmailpessoa());
            record.setIdcidade(pes.getIdcidade());
            record.setIdescolaridade(pes.getIdescolaridade());
            record.setIdestadocivil(pes.getIdestadocivil());
            record.setIdgenero(pes.getIdgenero());
            record.setNomepessoa(pes.getNomepessoa());

            Pessoa updated = pessoaRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Pessoa> findByCodigo(@PathVariable Integer codigo) {
        try {
            Pessoa cargo = pessoaRepository.findById(codigo).get();

            return ResponseEntity.ok(cargo);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        pessoaRepository.deleteById(codigo);
    }
}
package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.LancAvulso;
import br.com.empresa.pratica2.repository.LancAvulsoRepository;

@RestController
@RequestMapping("lancavulso")
public class LancAvulsoResource {
    @Autowired
    private LancAvulsoRepository lancavulsoRepository;

    @GetMapping
    public List<LancAvulso> findAll() {
        return lancavulsoRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<LancAvulso> salvar(@Valid @RequestBody LancAvulso lancavulso) {
        LancAvulso lancavulsoSalvo = lancavulsoRepository.save(lancavulso);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(lancavulsoSalvo.getIdlancamento()).toUri();

        return ResponseEntity.created(location).body(lancavulsoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<LancAvulso> alterar(@PathVariable Integer id, @Valid @RequestBody LancAvulso lancavulso) {

        return lancavulsoRepository.findById(id).map(record -> {

            record.setIdevento(lancavulso.getIdevento());
            record.setIdfuncionario(lancavulso.getIdfuncionario());
            record.setReferencia(lancavulso.getReferencia());
            record.setReferencia(lancavulso.getReferencia());
            record.setValor(lancavulso.getValor());
            LancAvulso updated = lancavulsoRepository.save(record);
 
            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<LancAvulso> findByCodigo(@PathVariable Integer codigo) {
        try {
            LancAvulso lancavulso = lancavulsoRepository.findById(codigo).get();

            return ResponseEntity.ok(lancavulso);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        lancavulsoRepository.deleteById(codigo);
    }
}
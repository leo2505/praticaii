package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import br.com.empresa.pratica2.model.Evento;
import br.com.empresa.pratica2.repository.EventoRepository;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("evento")
public class EventoResource {
    @Autowired
    private EventoRepository eventosRepository;

    @GetMapping
    public List<Evento> findAll() {
        return eventosRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Evento> salvar(@Valid @RequestBody Evento eventos) {
        Evento eventoSalvo = eventosRepository.save(eventos);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(eventoSalvo.getIdevento()).toUri();

        return ResponseEntity.created(location).body(eventoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Evento> alterar(@PathVariable Integer id, @Valid @RequestBody Evento eve) {

        return eventosRepository.findById(id).map(record -> {

            record.setDescricaoevento(eve.getDescricaoevento());
            record.setEventofixo(eve.getEventofixo());
            record.setIncidefgts(eve.getIncidefgts());
            record.setIncideinss(eve.getIncideinss());
            record.setIncideir(eve.getIncideir());
            record.setTipoevento(eve.getTipoevento());
            Evento updated = eventosRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Evento> findByCodigo(@PathVariable Integer codigo) {
        try {
            Evento eventos = eventosRepository.findById(codigo).get();

            return ResponseEntity.ok(eventos);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        eventosRepository.deleteById(codigo);
    } 
}

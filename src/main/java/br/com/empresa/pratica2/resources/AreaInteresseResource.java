package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.AreaInteresse;
import br.com.empresa.pratica2.repository.AreaInteresseRespository;

@RestController
@RequestMapping("areainteresse")
public class AreaInteresseResource {
    @Autowired
    private AreaInteresseRespository areaInteresseRepository;

    @GetMapping
    public List<AreaInteresse> findAll() {
        return areaInteresseRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<AreaInteresse> salvar(@RequestBody AreaInteresse area) {
        AreaInteresse areaSalvo = areaInteresseRepository.save(area);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(areaSalvo.getIdareainteresse()).toUri();

        return ResponseEntity.created(location).body(areaSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AreaInteresse> alterar(@PathVariable Integer id,
     @RequestBody AreaInteresse area) {

        return areaInteresseRepository.findById(id).map(record -> {

            record.setDescricaointeresse(area.getDescricaointeresse());
            AreaInteresse updated = areaInteresseRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<AreaInteresse> findByCodigo(@PathVariable Integer codigo) {
        try {
            AreaInteresse area = areaInteresseRepository.findById(codigo).get();

            return ResponseEntity.ok(area);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        areaInteresseRepository.deleteById(codigo);
    }

}
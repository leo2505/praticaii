package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Genero;
import br.com.empresa.pratica2.repository.GeneroRepository;

@RestController
@RequestMapping("genero")
public class GeneroResource {
    @Autowired
    private GeneroRepository generoRepository;

    @GetMapping
    public List<Genero> findAll() {
        return generoRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Genero> salvar(@Valid @RequestBody Genero genero) {
        Genero generoSalvo = generoRepository.save(genero);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(generoSalvo.getIdgenero()).toUri();

        return ResponseEntity.created(location).body(generoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Genero> alterar(@PathVariable Integer id, @Valid @RequestBody Genero gen) {

        return generoRepository.findById(id).map(record -> {

            record.setDescricaogenero(gen.getDescricaogenero());
            Genero updated = generoRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Genero> findByCodigo(@PathVariable Integer codigo) {
        try {
            Genero genero = generoRepository.findById(codigo).get();

            return ResponseEntity.ok(genero);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        generoRepository.deleteById(codigo);
    }

}
package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Irrf;
import br.com.empresa.pratica2.repository.IrrfRepository;

@RestController
@RequestMapping("irrf")
public class IrrfResource {
    @Autowired
    private IrrfRepository irrfRepository;

    @GetMapping
    public List<Irrf> findAll() {
        return irrfRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Irrf> salvar(@Valid @RequestBody Irrf irrf) {
        Irrf irrfSalvo = irrfRepository.save(irrf);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(irrfSalvo.getIdirrf()).toUri();

        return ResponseEntity.created(location).body(irrfSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Irrf> alterar(@PathVariable Integer id, @Valid @RequestBody Irrf irrf) {

        return irrfRepository.findById(id).map(record -> {

            record.setAliquota(irrf.getAliquota());
            record.setDeducaodependente(irrf.getDeducaodependente());
            record.setDeducaominima(irrf.getDeducaominima());
            record.setValidade(irrf.getValidade());
            record.setValorfinal(irrf.getValorfinal());
            record.setValorinicial(irrf.getValorinicial());

            Irrf updated = irrfRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Irrf> findByCodigo(@PathVariable Integer codigo) {
        try {
            Irrf irrf = irrfRepository.findById(codigo).get();

            return ResponseEntity.ok(irrf);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        irrfRepository.deleteById(codigo);
    }
}
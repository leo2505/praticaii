package br.com.empresa.pratica2.resources;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import javax.sql.DataSource;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import br.com.empresa.pratica2.model.Funcionario;
import br.com.empresa.pratica2.model.Horario;
import br.com.empresa.pratica2.model.HorarioFuncionario;
import br.com.empresa.pratica2.model.Inss;
import br.com.empresa.pratica2.model.Irrf;
import br.com.empresa.pratica2.model.LancAvulso;
import br.com.empresa.pratica2.repository.EventoFixoRepository;
import br.com.empresa.pratica2.repository.EventoFuncionarioRepository;
import br.com.empresa.pratica2.repository.EventoRepository;
import br.com.empresa.pratica2.repository.FuncionarioRepository;
import br.com.empresa.pratica2.repository.HorarioFuncionarioRepository;
import br.com.empresa.pratica2.repository.HorarioRepository;
import br.com.empresa.pratica2.repository.InssRepository;
import br.com.empresa.pratica2.repository.IrrfRepository;
import br.com.empresa.pratica2.repository.LancAvulsoRepository;
import br.com.empresa.pratica2.model.Calculo;
import br.com.empresa.pratica2.model.Evento;
import br.com.empresa.pratica2.model.EventoFixo;
import br.com.empresa.pratica2.model.EventoFuncionario;
import br.com.empresa.pratica2.repository.CalculoRepository;
import br.com.empresa.pratica2.repository.DependenteRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("calculo")
public class CalculoResource {
    @Autowired
    private CalculoRepository calculoRepository;

    @Autowired
    private HorarioFuncionarioRepository horariofunRepository;

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private InssRepository inssRepository;

    @Autowired
    private HorarioRepository horarioRepository;

    @Autowired
    private EventoFixoRepository eventoFixoRepository;

    @Autowired
    private LancAvulsoRepository lancAvulsoRepository;

    @Autowired
    private EventoFuncionarioRepository eventoFuncionarioRepository;

    @Autowired
    private DependenteRepository dependenteRepository;

    @Autowired
    private IrrfRepository irrfRepository;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private DataSource dataSource;

    @GetMapping
    public List<Calculo> findAll() {
        return calculoRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Calculo> salvar(@Valid @RequestBody Calculo calculo) {
        Calculo calculoSalvo = calculoRepository.save(calculo);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(calculoSalvo.getIdcalculo()).toUri();

        return ResponseEntity.created(location).body(calculoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Calculo> alterar(@PathVariable Integer id, @Valid @RequestBody Calculo calculo) {

        return calculoRepository.findById(id).map(record -> {

            record.setAno(calculo.getAno());
            record.setFechado(calculo.getFechado());
            record.setIdevento(calculo.getIdevento());
            record.setIdfuncionario(calculo.getIdfuncionario());
            record.setMes(calculo.getMes());
            record.setReferencia(calculo.getReferencia());
            record.setTipofolha(calculo.getTipofolha());
            record.setValor(calculo.getValor());
            Calculo updated = calculoRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Calculo> findByCodigo(@PathVariable Integer codigo) {
        try {
            Calculo calculo = calculoRepository.findById(codigo).get();

            return ResponseEntity.ok(calculo);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        calculoRepository.deleteById(codigo);
    }

    public float horasFaltantes(Funcionario fun, int mes, int ano) {
        Horario hr = horarioRepository.findById(fun.getIdhorario().getIdhorario()).get();

        float salariobase = fun.getSalariofuncionario();
        int cargamensal = hr.getHorasmensal();

        int countHorasFaltantes = eventoFuncionarioRepository.CountEventoFuncionario(fun.getIdfuncionario(), mes, ano,
                3);

        float salarioatual = 0;

        if (countHorasFaltantes != 0) {
            EventoFuncionario eve = eventoFuncionarioRepository.EventoFuncionarioPorEvento(fun.getIdfuncionario(), mes,
                    ano, 3);

            float valorhorafuncionario = salariobase / cargamensal;

            if (eve.getQuantidade() != 0) {
                salvarCalculo(eve.getIdevento(), valorhorafuncionario * eve.getQuantidade(), mes, ano, fun);
            }

            salarioatual = valorhorafuncionario * (cargamensal - eve.getQuantidade());
        }

        return salarioatual;
    }

    @GetMapping("/salarioliquido")
    public ResponseEntity<byte[]> calculoInss(@RequestParam("idfuncionario") int idfuncionario,
            @RequestParam("mes") int mes, @RequestParam("ano") int ano) {
        int countCalculo = calculoRepository.CountCalculo(idfuncionario, mes, ano);

        if (countCalculo == 0) {

            Funcionario fun = funcionarioRepository.findById(idfuncionario).get();

            float valorbase = 0;
            float salarioComHorasFaltantes = horasFaltantes(fun, mes, ano);

            if (salarioComHorasFaltantes != fun.getSalariofuncionario() && salarioComHorasFaltantes != 0) {
                valorbase = salarioComHorasFaltantes;
            } else {
                valorbase = Valores(fun, mes, ano) + fun.getSalariofuncionario();
            }

            float proventosFixos = ProventosEventosFixos(idfuncionario, mes, ano);
            float proventosLancAvulso = ProventosLancAvulso(idfuncionario, mes, ano);

            valorbase += (proventosFixos + proventosLancAvulso);

            float descontosFixos = DescontosEventosFixos(idfuncionario, mes, ano);
            float descontosLancAvulso = DescontosLancAvulso(idfuncionario, mes, ano);
            valorbase -= (descontosFixos + descontosLancAvulso);

            Inss inss = inssRepository.buscaPorSalario((double) valorbase);

            float resultado = valorbase - (valorbase * (inss.getAliquota() / 100));

            Evento eve = eventoRepository.findById(8).get();

            salvarCalculo(eve, (valorbase * (inss.getAliquota() / 100)), mes, ano, fun);

            float valorIRRF = calculoIRRF(resultado, fun, mes, ano);

            Evento eve2 = eventoRepository.findById(6).get();

            salvarCalculo(eve2, fun.getSalariofuncionario(), mes, ano, fun);
            salvarProventosDescontosFixos(fun, mes, ano);
            salvarProventosDescontosAvulsos(fun, mes, ano);

            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idfuncionario", idfuncionario);
            parametros.put("mes", mes);
            parametros.put("ano", ano);
            parametros.put("valorirrf", "" + valorIRRF);

            byte[] reportBytes = null;

            try {
                InputStream io = this.getClass().getResourceAsStream("/reports/recibo_pagamento_report.jasper");
                JasperPrint jasperPrint = JasperFillManager.fillReport(io, parametros, dataSource.getConnection());

                reportBytes = JasperExportManager.exportReportToPdf(jasperPrint);
            } catch (JRException | SQLException ex) {
                return ResponseEntity.noContent().build();
            }

            return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"FolhaPagamento_" + mes + "/" + ano
                            + "_" + fun.getNomefuncionario() + ".pdf\"")
                    .body(reportBytes);
        } else {
            return ResponseEntity.ok().build();
        }
    }

    public void salvarProventosDescontosFixos(Funcionario fun, int mes, int ano) {
        List<EventoFixo> lista = eventoFixoRepository.ListaEventosFixo(fun.getIdfuncionario(), mes, ano);

        for (EventoFixo var : lista) {
            if (var.getValor() != 0)
                salvarCalculo(var.getIdevento(), var.getValor(), mes, ano, fun);
        }
    }

    public void salvarProventosDescontosAvulsos(Funcionario fun, int mes, int ano) {
        List<LancAvulso> lista = lancAvulsoRepository.ListaLancAvulso(fun.getIdfuncionario(), mes, ano);

        for (LancAvulso var : lista) {
            if (var.getValor() != 0)
                salvarCalculo(var.getIdevento(), var.getValor(), mes, ano, fun);
        }
    }

    public float ProventosEventosFixos(int idfuncionario, int mes, int ano) {
        float valorProvento = eventoFixoRepository.valorEvento(idfuncionario, mes, ano, "provento");

        return valorProvento;
    }

    public float DescontosEventosFixos(int idfuncionario, int mes, int ano) {
        float valorDesconto = eventoFixoRepository.valorEvento(idfuncionario, mes, ano, "desconto");

        return valorDesconto;
    }

    public float ProventosLancAvulso(int idfuncionario, int mes, int ano) {
        float valor = lancAvulsoRepository.valorLancAvulso(idfuncionario, "provento", mes, ano);
        return valor;
    }

    public float DescontosLancAvulso(int idfuncionario, int mes, int ano) {
        float valor = lancAvulsoRepository.valorLancAvulso(idfuncionario, "desconto", mes, ano);
        return valor;
    }

    public float Valores(Funcionario fun, int mes, int ano) {
        Horario hr = horarioRepository.findById(fun.getIdhorario().getIdhorario()).get();
        float valorDsr = 0;
        float valorHorasExtras = totalHoraExtraFuncionario(fun, hr, mes, ano);

        if (valorHorasExtras != 0) {
            HorarioFuncionario hf = horariofunRepository.buscaPorIdFuncionario(fun.getIdfuncionario(), mes, ano);

            valorDsr = Dsr(valorHorasExtras, hf);
            if (valorDsr != 0) {
                Evento eve = eventoRepository.findById(7).get();

                salvarCalculo(eve, valorDsr, mes, ano, fun);
            }
        }

        float resultado = valorHorasExtras + valorDsr;

        return resultado;
    }

    public float totalHoraExtraFuncionario(Funcionario fun, Horario hr, int mes, int ano) {
        float salariobase = fun.getSalariofuncionario();
        int cargamensal = hr.getHorasmensal();
        float totalRecebidoHoraExtra = 0;
        float valorhorafuncionario = salariobase / cargamensal;
        float valorhoraextraCorreta = 0;

        int countCinquenta = eventoFuncionarioRepository.CountEventoFuncionario(fun.getIdfuncionario(), mes, ano, 1);
        int countCem = eventoFuncionarioRepository.CountEventoFuncionario(fun.getIdfuncionario(), mes, ano, 2);

        if (countCinquenta != 0) {// 50%
            EventoFuncionario eve = eventoFuncionarioRepository.EventoFuncionarioPorEvento(fun.getIdfuncionario(), mes,
                    ano, 1);

            valorhoraextraCorreta = (valorhorafuncionario / 2) + valorhorafuncionario;

            totalRecebidoHoraExtra += eve.getQuantidade() * valorhoraextraCorreta;
            if (eve.getQuantidade() != 0)
                salvarCalculo(eve.getIdevento(), totalRecebidoHoraExtra, mes, ano, fun);
        }

        if (countCem != 0) {// 100%
            EventoFuncionario eve = eventoFuncionarioRepository.EventoFuncionarioPorEvento(fun.getIdfuncionario(), mes,
                    ano, 2);

            valorhoraextraCorreta = valorhorafuncionario + valorhorafuncionario;

            totalRecebidoHoraExtra += eve.getQuantidade() * valorhoraextraCorreta;
            if (eve.getQuantidade() != 0)
                salvarCalculo(eve.getIdevento(), eve.getQuantidade() * valorhoraextraCorreta, mes, ano, fun);
        }

        return totalRecebidoHoraExtra;
    }

    public float Dsr(float totalhoras, HorarioFuncionario hf) {
        int diasuteis = hf.getDiasuteis();
        int diasnaouteis = hf.getDiasnaouteis();

        return (totalhoras / diasuteis) * diasnaouteis;
    }

    public float calculoIRRF(float valorbase, Funcionario fun, int mes, int ano) {

        int countDependentes = dependenteRepository.CountDependentesFuncionario(fun.getIdfuncionario());

        if (countDependentes != 0) {
            double valor = 189.59 * countDependentes;

            valorbase = valorbase - (float) valor;
        }
        if (valorbase >= 1903.99) {

            Irrf irrf = irrfRepository.buscaPorSalario((double) valorbase);

            float valor = (valorbase * (irrf.getAliquota() / 100));

            valor -= irrf.getDeducaominima();

            Evento eve = eventoRepository.findById(9).get();

            salvarCalculo(eve, valor, mes, ano, fun);

            return valorbase;
        } else {
            return 0;
        }
    }

    public void salvarCalculo(Evento eve, float valor, int mes, int ano, Funcionario fun) {
        Calculo cal = new Calculo();
        cal.setIdevento(eve);
        cal.setReferencia("");
        cal.setValor(valor);
        cal.setMes(mes);
        cal.setAno(ano);
        cal.setTipofolha("Pagamento Mensal");
        cal.setIdfuncionario(fun);
        cal.setFechado(1);

        calculoRepository.save(cal);
    }
}
package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.EventoFuncionario;
import br.com.empresa.pratica2.repository.EventoFuncionarioRepository;

@RestController
@RequestMapping("eventofuncionario")
public class EventoFuncionarioResource {
    @Autowired
    private EventoFuncionarioRepository eventofuncionarioRepository;

    @GetMapping
    public List<EventoFuncionario> findAll() {
        return eventofuncionarioRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<EventoFuncionario> salvar(@RequestBody EventoFuncionario eve) {
        EventoFuncionario eventoSalvo = eventofuncionarioRepository.save(eve);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(eventoSalvo.getIdeventofuncionario()).toUri();

        return ResponseEntity.created(location).body(eventoSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventoFuncionario> alterar(@PathVariable Integer id, @RequestBody EventoFuncionario eve) {

        return eventofuncionarioRepository.findById(id).map(record -> {

            record.setAno(eve.getAno());
            record.setIdevento(eve.getIdevento());
            record.setIdfuncionario(eve.getIdfuncionario());
            record.setMes(eve.getMes());
            record.setValordesconto(eve.getValordesconto());
            record.setValorprovento(eve.getValorprovento());
            record.setQuantidade(eve.getQuantidade());
            EventoFuncionario updated = eventofuncionarioRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<EventoFuncionario> findByCodigo(@PathVariable Integer codigo) {
        try {
            EventoFuncionario eve = eventofuncionarioRepository.findById(codigo).get();

            return ResponseEntity.ok(eve);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        eventofuncionarioRepository.deleteById(codigo);
    }

}
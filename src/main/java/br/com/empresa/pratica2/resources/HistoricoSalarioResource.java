package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.HistoricoSalario;
import br.com.empresa.pratica2.repository.HistoricoSalarioRepository;

@RestController
@RequestMapping("historicosalario")
public class HistoricoSalarioResource {
    @Autowired
    private HistoricoSalarioRepository histRepository;

    @GetMapping
    public List<HistoricoSalario> findAll() {
        return histRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<HistoricoSalario> salvar(@RequestBody HistoricoSalario hist) {
        HistoricoSalario histSalvo = histRepository.save(hist);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(histSalvo.getIdhistsalario()).toUri();

        return ResponseEntity.created(location).body(histSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HistoricoSalario> alterar(@PathVariable Integer id, @RequestBody HistoricoSalario hist) {

        return histRepository.findById(id).map(record -> {

            record.setDatafinal(hist.getDatafinal());
            record.setDatainicial(hist.getDatainicial());
            record.setIdfuncionario(hist.getIdfuncionario());
            record.setMotivoalteracao(hist.getMotivoalteracao());
            record.setValorsalario(hist.getValorsalario());

            HistoricoSalario updated = histRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<HistoricoSalario> findByCodigo(@PathVariable Integer codigo) {
        try {
            HistoricoSalario hist = histRepository.findById(codigo).get();

            return ResponseEntity.ok(hist);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        histRepository.deleteById(codigo);
    }

}
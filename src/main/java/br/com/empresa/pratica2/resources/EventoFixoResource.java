package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.EventoFixo;
import br.com.empresa.pratica2.repository.EventoFixoRepository;

@RestController
@RequestMapping("eventofixo")
public class EventoFixoResource {
    @Autowired
    private EventoFixoRepository eveRepository;

    @GetMapping
    public List<EventoFixo> findAll() {
        return eveRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<EventoFixo> salvar(@RequestBody EventoFixo eve) {
        EventoFixo eveSalvo = eveRepository.save(eve);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(eveSalvo.getIdeventofixo()).toUri();

        return ResponseEntity.created(location).body(eveSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventoFixo> alterar(@PathVariable Integer id, @RequestBody EventoFixo eve) {

        return eveRepository.findById(id).map(record -> {

            record.setData_fim(eve.getData_fim());
            record.setData_inicio(eve.getData_inicio());
            record.setIdevento(eve.getIdevento());
            record.setIdfuncionario(eve.getIdfuncionario());
            record.setReferencia(eve.getReferencia());
            record.setValor(eve.getValor());
            EventoFixo updated = eveRepository.save(record);

            return ResponseEntity.ok().body(updated);
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<EventoFixo> findByCodigo(@PathVariable Integer codigo) {
        try {
            EventoFixo eve = eveRepository.findById(codigo).get();

            return ResponseEntity.ok(eve);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        eveRepository.deleteById(codigo);
    }

}
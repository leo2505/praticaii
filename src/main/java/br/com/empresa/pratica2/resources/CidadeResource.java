package br.com.empresa.pratica2.resources;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.empresa.pratica2.model.Cidade;
import br.com.empresa.pratica2.repository.CidadeRepository;

@RestController
@RequestMapping("cidade")
public class CidadeResource {
    @Autowired
    private CidadeRepository cidadeRepository;

    @GetMapping
    public List<Cidade> findAll() {
        return cidadeRepository.findAll();
    }
    
    @PostMapping
    public ResponseEntity<Cidade> salvar(@Valid @RequestBody Cidade cidade) {
        Cidade cidadeSalvo = cidadeRepository.save(cidade);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{codigo}")
                .buildAndExpand(cidadeSalvo.getIdcidade()).toUri();

        return ResponseEntity.created(location).body(cidadeSalvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Cidade> alterar(@PathVariable Integer id, @Valid @RequestBody Cidade cidade) {

        return cidadeRepository.findById(id).map(record -> {

            record.setCepcidade(cidade.getCepcidade());
            record.setDescricaocidade(cidade.getDescricaocidade());
            Cidade updated = cidadeRepository.save(record);

            return ResponseEntity.ok().body(updated); 
        }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Cidade> findByCodigo(@PathVariable Integer codigo) {
        try {
            Cidade cidade = cidadeRepository.findById(codigo).get();

            return ResponseEntity.ok(cidade);
        } catch (NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer codigo) {
        cidadeRepository.deleteById(codigo);
    }
}
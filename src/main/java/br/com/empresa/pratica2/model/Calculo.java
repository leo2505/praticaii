package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "calculo")
public class Calculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idcalculo;
    private String referencia;
    private float valor;
    private int mes;
    private int ano;
    private String tipofolha;
    private int fechado;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    @ManyToOne
    @JoinColumn(name = "idevento")
    private Evento idevento;

    public int getIdcalculo() {
        return this.idcalculo;
    }

    public void setIdcalculo(int idcalculo) {
        this.idcalculo = idcalculo;
    }

    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public float getValor() {
        return this.valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getMes() {
        return this.mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return this.ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getTipofolha() {
        return this.tipofolha;
    }

    public void setTipofolha(String tipofolha) {
        this.tipofolha = tipofolha;
    }

    public int getFechado() {
        return this.fechado;
    }

    public void setFechado(int fechado) {
        this.fechado = fechado;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Evento getIdevento() {
        return this.idevento;
    }

    public void setIdevento(Evento idevento) {
        this.idevento = idevento;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Calculo)) {
            return false;
        }
        Calculo calculo = (Calculo) o;
        return idcalculo == calculo.idcalculo && Objects.equals(referencia, calculo.referencia)
                && valor == calculo.valor && mes == calculo.mes && ano == calculo.ano
                && Objects.equals(tipofolha, calculo.tipofolha) && fechado == calculo.fechado
                && Objects.equals(idfuncionario, calculo.idfuncionario) && Objects.equals(idevento, calculo.idevento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcalculo, referencia, valor, mes, ano, tipofolha, fechado, idfuncionario, idevento);
    }
}
package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "inss")
public class Inss {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idinss;
    @NotNull
    private double valorinicial;
    @NotNull
    private double valorfinal;
    @NotNull
    private float aliquota;
    private Date validade;

    public int getIdinss() {
        return this.idinss;
    }

    public void setIdinss(int idinss) {
        this.idinss = idinss;
    }

    public double getValorinicial() {
        return this.valorinicial;
    }

    public void setValorinicial(double valorinicial) {
        this.valorinicial = valorinicial;
    }

    public double getValorfinal() {
        return this.valorfinal;
    }

    public void setValorfinal(double valorfinal) {
        this.valorfinal = valorfinal;
    }

    public float getAliquota() {
        return this.aliquota;
    }

    public void setAliquota(float aliquota) {
        this.aliquota = aliquota;
    }

    public Date getValidade() {
        return this.validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Inss)) {
            return false;
        }
        Inss inss = (Inss) o;
        return idinss == inss.idinss && valorinicial == inss.valorinicial && valorfinal == inss.valorfinal
                && aliquota == inss.aliquota && Objects.equals(validade, inss.validade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idinss, valorinicial, valorfinal, aliquota, validade);
    }
}
package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eventofixo")
public class EventoFixo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ideventofixo;

    @ManyToOne
    @JoinColumn(name = "idevento")
    private Evento idevento;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    private String referencia;
    private float valor;
    private Date data_inicio;
    private Date data_fim;

    public int getIdeventofixo() {
        return this.ideventofixo;
    }

    public void setIdeventofixo(int ideventofixo) {
        this.ideventofixo = ideventofixo;
    }

    public Evento getIdevento() {
        return this.idevento;
    }

    public void setIdevento(Evento idevento) {
        this.idevento = idevento;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public float getValor() {
        return this.valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Date getData_inicio() {
        return this.data_inicio;
    }

    public void setData_inicio(Date data_inicio) {
        this.data_inicio = data_inicio;
    }

    public Date getData_fim() {
        return this.data_fim;
    }

    public void setData_fim(Date data_fim) {
        this.data_fim = data_fim;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof EventoFixo)) {
            return false;
        }
        EventoFixo eventoFixo = (EventoFixo) o;
        return ideventofixo == eventoFixo.ideventofixo && Objects.equals(idevento, eventoFixo.idevento)
                && Objects.equals(idfuncionario, eventoFixo.idfuncionario)
                && Objects.equals(referencia, eventoFixo.referencia) && valor == eventoFixo.valor
                && Objects.equals(data_inicio, eventoFixo.data_inicio) && Objects.equals(data_fim, eventoFixo.data_fim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideventofixo, idevento, idfuncionario, referencia, valor, data_inicio, data_fim);
    }
}
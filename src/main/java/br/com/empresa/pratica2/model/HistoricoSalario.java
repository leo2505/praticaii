package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "historicosalario")
public class HistoricoSalario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idhistsalario;
    private Date datainicial;
    private Date datafinal;
    private float valorsalario;
    private String motivoalteracao;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    public int getIdhistsalario() {
        return this.idhistsalario;
    }

    public void setIdhistsalario(int idhistsalario) {
        this.idhistsalario = idhistsalario;
    }

    public Date getDatainicial() {
        return this.datainicial;
    }

    public void setDatainicial(Date datainicial) {
        this.datainicial = datainicial;
    }

    public Date getDatafinal() {
        return this.datafinal;
    }

    public void setDatafinal(Date datafinal) {
        this.datafinal = datafinal;
    }

    public float getValorsalario() {
        return this.valorsalario;
    }

    public void setValorsalario(float valorsalario) {
        this.valorsalario = valorsalario;
    }

    public String getMotivoalteracao() {
        return this.motivoalteracao;
    }

    public void setMotivoalteracao(String motivoalteracao) {
        this.motivoalteracao = motivoalteracao;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof HistoricoSalario)) {
            return false;
        }
        HistoricoSalario historicoSalario = (HistoricoSalario) o;
        return idhistsalario == historicoSalario.idhistsalario
                && Objects.equals(datainicial, historicoSalario.datainicial)
                && Objects.equals(datafinal, historicoSalario.datafinal)
                && valorsalario == historicoSalario.valorsalario
                && Objects.equals(motivoalteracao, historicoSalario.motivoalteracao)
                && Objects.equals(idfuncionario, historicoSalario.idfuncionario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhistsalario, datainicial, datafinal, valorsalario, motivoalteracao, idfuncionario);
    }
}
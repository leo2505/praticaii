package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

@Entity
@Table(name = "funcionario")
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idfuncionario;

    private String nomefuncionario;
    private String logradourofuncionario;
    @NotNull
    private String enderecofuncionario;
    private String complementoendereco;
    private String emailfuncionario;
    @NotNull
    private String telefonefuncionario;
    @NotNull
    @CPF
    private String cpffuncionario;
    @NotNull
    private String rgfuncionario;
    private String expedicaorgfuncionario;
    private String orgaorgfuncionario;
    private String ufrgfuncionario;
    private String ctpsseriefuncionario;
    private String ctpsexpedicaofuncionario;
    private String ctpsfuncionario;
    @NotNull
    private String pisfuncionario;
    private String cnhfuncionario;
    private String cnhexpedicaofuncionario;
    private String cnhcategoriafuncionario;
    private String cnhvencimentofuncionario;
    private String cnh1habilitacaofuncionario;
    @NotNull
    private float salariofuncionario;
    @NotNull
    private String codbancofuncionario;
    @NotNull
    private String numcontabancofuncionario;
    @NotNull
    private String agenciabancofuncionario;
    @NotNull
    private Date dataadmissaofuncionario;
    @NotNull
    private String contafgtsfuncionario;
    private String nomepaifuncionario;
    private String nomemaefuncionario;

    @ManyToOne
    @JoinColumn(name = "idpessoa")
    private Pessoa idpessoa;

    @ManyToOne
    @JoinColumn(name = "idcargo")
    private Cargo idcargo;

    @ManyToOne
    @JoinColumn(name = "idsetor")
    private Setor idsetor;

    @ManyToOne
    @JoinColumn(name = "idhorario")
    private Horario idhorario;

    public int getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(int idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public String getNomefuncionario() {
        return this.nomefuncionario;
    }

    public void setNomefuncionario(String nomefuncionario) {
        this.nomefuncionario = nomefuncionario;
    }

    public String getLogradourofuncionario() {
        return this.logradourofuncionario;
    }

    public void setLogradourofuncionario(String logradourofuncionario) {
        this.logradourofuncionario = logradourofuncionario;
    }

    public String getEnderecofuncionario() {
        return this.enderecofuncionario;
    }

    public void setEnderecofuncionario(String enderecofuncionario) {
        this.enderecofuncionario = enderecofuncionario;
    }

    public String getCtpsfuncionario() {
        return this.ctpsfuncionario;
    }

    public void setCtpsfuncionario(String ctpsfuncionario) {
        this.ctpsfuncionario = ctpsfuncionario;
    }

    public String getComplementoendereco() {
        return this.complementoendereco;
    }

    public void setComplementoendereco(String complementoendereco) {
        this.complementoendereco = complementoendereco;
    }

    public String getEmailfuncionario() {
        return this.emailfuncionario;
    }

    public void setEmailfuncionario(String emailfuncionario) {
        this.emailfuncionario = emailfuncionario;
    }

    public String getTelefonefuncionario() {
        return this.telefonefuncionario;
    }

    public void setTelefonefuncionario(String telefonefuncionario) {
        this.telefonefuncionario = telefonefuncionario;
    }

    public String getCpffuncionario() {
        return this.cpffuncionario;
    }

    public void setCpffuncionario(String cpffuncionario) {
        this.cpffuncionario = cpffuncionario;
    }

    public String getRgfuncionario() {
        return this.rgfuncionario;
    }

    public void setRgfuncionario(String rgfuncionario) {
        this.rgfuncionario = rgfuncionario;
    }

    public String getExpedicaorgfuncionario() {
        return this.expedicaorgfuncionario;
    }

    public void setExpedicaorgfuncionario(String expedicaorgfuncionario) {
        this.expedicaorgfuncionario = expedicaorgfuncionario;
    }

    public String getOrgaorgfuncionario() {
        return this.orgaorgfuncionario;
    }

    public void setOrgaorgfuncionario(String orgaorgfuncionario) {
        this.orgaorgfuncionario = orgaorgfuncionario;
    }

    public String getUfrgfuncionario() {
        return this.ufrgfuncionario;
    }

    public void setUfrgfuncionario(String ufrgfuncionario) {
        this.ufrgfuncionario = ufrgfuncionario;
    }

    public String getCtpsseriefuncionario() {
        return this.ctpsseriefuncionario;
    }

    public void setCtpsseriefuncionario(String ctpsseriefuncionario) {
        this.ctpsseriefuncionario = ctpsseriefuncionario;
    }

    public String getCtpsexpedicaofuncionario() {
        return this.ctpsexpedicaofuncionario;
    }

    public void setCtpsexpedicaofuncionario(String ctpsexpedicaofuncionario) {
        this.ctpsexpedicaofuncionario = ctpsexpedicaofuncionario;
    }

    public String getPisfuncionario() {
        return this.pisfuncionario;
    }

    public void setPisfuncionario(String pisfuncionario) {
        this.pisfuncionario = pisfuncionario;
    }

    public String getCnhfuncionario() {
        return this.cnhfuncionario;
    }

    public void setCnhfuncionario(String cnhfuncionario) {
        this.cnhfuncionario = cnhfuncionario;
    }

    public String getCnhexpedicaofuncionario() {
        return this.cnhexpedicaofuncionario;
    }

    public void setCnhexpedicaofuncionario(String cnhexpedicaofuncionario) {
        this.cnhexpedicaofuncionario = cnhexpedicaofuncionario;
    }

    public String getCnhcategoriafuncionario() {
        return this.cnhcategoriafuncionario;
    }

    public void setCnhcategoriafuncionario(String cnhcategoriafuncionario) {
        this.cnhcategoriafuncionario = cnhcategoriafuncionario;
    }

    public String getCnhvencimentofuncionario() {
        return this.cnhvencimentofuncionario;
    }

    public void setCnhvencimentofuncionario(String cnhvencimentofuncionario) {
        this.cnhvencimentofuncionario = cnhvencimentofuncionario;
    }

    public String getCnh1habilitacaofuncionario() {
        return this.cnh1habilitacaofuncionario;
    }

    public void setCnh1habilitacaofuncionario(String cnh1habilitacaofuncionario) {
        this.cnh1habilitacaofuncionario = cnh1habilitacaofuncionario;
    }

    public float getSalariofuncionario() {
        return this.salariofuncionario;
    }

    public void setSalariofuncionario(float salariofuncionario) {
        this.salariofuncionario = salariofuncionario;
    }

    public String getCodbancofuncionario() {
        return this.codbancofuncionario;
    }

    public void setCodbancofuncionario(String codbancofuncionario) {
        this.codbancofuncionario = codbancofuncionario;
    }

    public String getNumcontabancofuncionario() {
        return this.numcontabancofuncionario;
    }

    public void setNumcontabancofuncionario(String numcontabancofuncionario) {
        this.numcontabancofuncionario = numcontabancofuncionario;
    }

    public String getAgenciabancofuncionario() {
        return this.agenciabancofuncionario;
    }

    public void setAgenciabancofuncionario(String agenciabancofuncionario) {
        this.agenciabancofuncionario = agenciabancofuncionario;
    }

    public Date getDataadmissaofuncionario() {
        return this.dataadmissaofuncionario;
    }

    public void setDataadmissaofuncionario(Date dataadmissaofuncionario) {
        this.dataadmissaofuncionario = dataadmissaofuncionario;
    }

    public String getContafgtsfuncionario() {
        return this.contafgtsfuncionario;
    }

    public void setContafgtsfuncionario(String contafgtsfuncionario) {
        this.contafgtsfuncionario = contafgtsfuncionario;
    }

    public String getNomepaifuncionario() {
        return this.nomepaifuncionario;
    }

    public void setNomepaifuncionario(String nomepaifuncionario) {
        this.nomepaifuncionario = nomepaifuncionario;
    }

    public String getNomemaefuncionario() {
        return this.nomemaefuncionario;
    }

    public void setNomemaefuncionario(String nomemaefuncionario) {
        this.nomemaefuncionario = nomemaefuncionario;
    }

    public Pessoa getIdpessoa() {
        return this.idpessoa;
    }

    public void setIdpessoa(Pessoa idpessoa) {
        this.idpessoa = idpessoa;
    }

    public Cargo getIdcargo() {
        return this.idcargo;
    }

    public void setIdcargo(Cargo idcargo) {
        this.idcargo = idcargo;
    }

    public Setor getIdsetor() {
        return this.idsetor;
    }

    public void setIdsetor(Setor idsetor) {
        this.idsetor = idsetor;
    }

    public Horario getIdhorario() {
        return this.idhorario;
    }

    public void setIdhorario(Horario idhorario) {
        this.idhorario = idhorario;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Funcionario)) {
            return false;
        }
        Funcionario funcionario = (Funcionario) o;
        return idfuncionario == funcionario.idfuncionario
                && Objects.equals(nomefuncionario, funcionario.nomefuncionario)
                && Objects.equals(logradourofuncionario, funcionario.logradourofuncionario)
                && Objects.equals(enderecofuncionario, funcionario.enderecofuncionario)
                && Objects.equals(complementoendereco, funcionario.complementoendereco)
                && Objects.equals(emailfuncionario, funcionario.emailfuncionario)
                && Objects.equals(telefonefuncionario, funcionario.telefonefuncionario)
                && Objects.equals(cpffuncionario, funcionario.cpffuncionario)
                && Objects.equals(rgfuncionario, funcionario.rgfuncionario)
                && Objects.equals(expedicaorgfuncionario, funcionario.expedicaorgfuncionario)
                && Objects.equals(orgaorgfuncionario, funcionario.orgaorgfuncionario)
                && Objects.equals(ufrgfuncionario, funcionario.ufrgfuncionario)
                && Objects.equals(ctpsseriefuncionario, funcionario.ctpsseriefuncionario)
                && Objects.equals(ctpsexpedicaofuncionario, funcionario.ctpsexpedicaofuncionario)
                && Objects.equals(pisfuncionario, funcionario.pisfuncionario)
                && Objects.equals(cnhfuncionario, funcionario.cnhfuncionario)
                && Objects.equals(cnhexpedicaofuncionario, funcionario.cnhexpedicaofuncionario)
                && Objects.equals(cnhcategoriafuncionario, funcionario.cnhcategoriafuncionario)
                && Objects.equals(cnhvencimentofuncionario, funcionario.cnhvencimentofuncionario)
                && Objects.equals(cnh1habilitacaofuncionario, funcionario.cnh1habilitacaofuncionario)
                && salariofuncionario == funcionario.salariofuncionario
                && Objects.equals(codbancofuncionario, funcionario.codbancofuncionario)
                && Objects.equals(numcontabancofuncionario, funcionario.numcontabancofuncionario)
                && Objects.equals(agenciabancofuncionario, funcionario.agenciabancofuncionario)
                && Objects.equals(dataadmissaofuncionario, funcionario.dataadmissaofuncionario)
                && Objects.equals(contafgtsfuncionario, funcionario.contafgtsfuncionario)
                && Objects.equals(nomepaifuncionario, funcionario.nomepaifuncionario)
                && Objects.equals(nomemaefuncionario, funcionario.nomemaefuncionario)
                && Objects.equals(idpessoa, funcionario.idpessoa) && Objects.equals(idcargo, funcionario.idcargo)
                && Objects.equals(idsetor, funcionario.idsetor) && Objects.equals(idhorario, funcionario.idhorario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idfuncionario, nomefuncionario, logradourofuncionario, enderecofuncionario,
                complementoendereco, emailfuncionario, telefonefuncionario, cpffuncionario, rgfuncionario,
                expedicaorgfuncionario, orgaorgfuncionario, ufrgfuncionario, ctpsseriefuncionario,
                ctpsexpedicaofuncionario, pisfuncionario, cnhfuncionario, cnhexpedicaofuncionario,
                cnhcategoriafuncionario, cnhvencimentofuncionario, cnh1habilitacaofuncionario, salariofuncionario,
                codbancofuncionario, numcontabancofuncionario, agenciabancofuncionario, dataadmissaofuncionario,
                contafgtsfuncionario, nomepaifuncionario, nomemaefuncionario, idpessoa, idcargo, idsetor, idhorario);
    }
}
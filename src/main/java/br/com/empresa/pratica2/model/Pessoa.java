package br.com.empresa.pratica2.model;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

@Entity
@Table(name = "pessoa")
public class Pessoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idpessoa;
    @NotNull
    private String nomepessoa;
    @NotNull
    private Date datanascimentopessoa;
    private String emailpessoa;
    @NotNull
    private String contatopessoa;
    @NotNull
    @CPF
    private String cpfpessoa;

    @ManyToOne
    @JoinColumn(name = "idestadocivil")
    private EstadoCivil idestadocivil;

    //@NotNull
    @ManyToOne
    @JoinColumn(name = "idescolaridade")
    private Escolaridade idescolaridade;

    @ManyToOne
    @JoinColumn(name = "idcidade")
    private Cidade idcidade;

    @ManyToOne
    @JoinColumn(name = "idgenero")
    private Genero idgenero;

    public int getIdpessoa() {
        return this.idpessoa;
    }

    public void setIdpessoa(int idpessoa) {
        this.idpessoa = idpessoa;
    }

    public String getNomepessoa() {
        return this.nomepessoa;
    }

    public void setNomepessoa(String nomepessoa) {
        this.nomepessoa = nomepessoa;
    }

    public Date getDatanascimentopessoa() {
        return this.datanascimentopessoa;
    }

    public void setDatanascimentopessoa(Date datanascimentopessoa) {
        this.datanascimentopessoa = datanascimentopessoa;
    }

    public String getEmailpessoa() {
        return this.emailpessoa;
    }

    public void setEmailpessoa(String emailpessoa) {
        this.emailpessoa = emailpessoa;
    }

    public String getContatopessoa() {
        return this.contatopessoa;
    }

    public void setContatopessoa(String contatopessoa) {
        this.contatopessoa = contatopessoa;
    }

    public String getCpfpessoa() {
        return this.cpfpessoa;
    }

    public void setCpfpessoa(String cpfpessoa) {
        this.cpfpessoa = cpfpessoa;
    }

    public EstadoCivil getIdestadocivil() {
        return this.idestadocivil;
    }

    public void setIdestadocivil(EstadoCivil idestadocivil) {
        this.idestadocivil = idestadocivil;
    }

    public Escolaridade getIdescolaridade() {
        return this.idescolaridade;
    }

    public void setIdescolaridade(Escolaridade idescolaridade) {
        this.idescolaridade = idescolaridade;
    }

    public Cidade getIdcidade() {
        return this.idcidade;
    }

    public void setIdcidade(Cidade idcidade) {
        this.idcidade = idcidade;
    }

    public Genero getIdgenero() {
        return this.idgenero;
    }

    public void setIdgenero(Genero idgenero) {
        this.idgenero = idgenero;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Pessoa)) {
            return false;
        }
        Pessoa pessoa = (Pessoa) o;
        return idpessoa == pessoa.idpessoa && Objects.equals(nomepessoa, pessoa.nomepessoa) && Objects.equals(datanascimentopessoa, pessoa.datanascimentopessoa) && Objects.equals(emailpessoa, pessoa.emailpessoa) && Objects.equals(contatopessoa, pessoa.contatopessoa) && Objects.equals(cpfpessoa, pessoa.cpfpessoa) && Objects.equals(idestadocivil, pessoa.idestadocivil) && Objects.equals(idescolaridade, pessoa.idescolaridade) && Objects.equals(idcidade, pessoa.idcidade) && Objects.equals(idgenero, pessoa.idgenero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idpessoa, nomepessoa, datanascimentopessoa, emailpessoa, contatopessoa, cpfpessoa, idestadocivil, idescolaridade, idcidade, idgenero);
    }   
}
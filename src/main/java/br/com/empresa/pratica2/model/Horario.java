package br.com.empresa.pratica2.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "horario")
public class Horario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idhorario;
    private String descricaohorario;
    @NotNull
    private String inimanha;
    @NotNull
    private String fimmanha;

    private String initarde;
    private String fimtarde;
    private int horasmensal;

    public int getIdhorario() {
        return this.idhorario;
    }

    public void setIdhorario(int idhorario) {
        this.idhorario = idhorario;
    }

    public String getDescricaohorario() {
        return this.descricaohorario;
    }

    public void setDescricaohorario(String descricaohorario) {
        this.descricaohorario = descricaohorario;
    }

    public String getInimanha() {
        return this.inimanha;
    }

    public void setInimanha(String inimanha) {
        this.inimanha = inimanha;
    }

    public String getFimmanha() {
        return this.fimmanha;
    }

    public void setFimmanha(String fimmanha) {
        this.fimmanha = fimmanha;
    }

    public String getInitarde() {
        return this.initarde;
    }

    public void setInitarde(String initarde) {
        this.initarde = initarde;
    }

    public String getFimtarde() {
        return this.fimtarde;
    }

    public void setFimtarde(String fimtarde) {
        this.fimtarde = fimtarde;
    }

    public int getHorasmensal() {
        return this.horasmensal;
    }

    public void setHorasmensal(int horasmensal) {
        this.horasmensal = horasmensal;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Horario)) {
            return false;
        }
        Horario horario = (Horario) o;
        return idhorario == horario.idhorario && Objects.equals(descricaohorario, horario.descricaohorario)
                && Objects.equals(inimanha, horario.inimanha) && Objects.equals(fimmanha, horario.fimmanha)
                && Objects.equals(initarde, horario.initarde) && Objects.equals(fimtarde, horario.fimtarde)
                && horasmensal == horario.horasmensal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhorario, descricaohorario, inimanha, fimmanha, initarde, fimtarde, horasmensal);
    }
}
package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cidade")
public class Cidade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idcidade;
    @NotNull
    private String descricaocidade;
    private String cepcidade;

    public int getIdcidade() {
        return this.idcidade;
    }

    public void setIdcidade(int idcidade) {
        this.idcidade = idcidade;
    }

    public String getDescricaocidade() {
        return this.descricaocidade;
    }

    public void setDescricaocidade(String descricaocidade) {
        this.descricaocidade = descricaocidade;
    }

    public String getCepcidade() {
        return this.cepcidade;
    }

    public void setCepcidade(String cepcidade) {
        this.cepcidade = cepcidade;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Cidade)) {
            return false;
        }
        Cidade cidade = (Cidade) o;
        return idcidade == cidade.idcidade && Objects.equals(descricaocidade, cidade.descricaocidade) && Objects.equals(cepcidade, cidade.cepcidade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcidade, descricaocidade, cepcidade);
    }
}
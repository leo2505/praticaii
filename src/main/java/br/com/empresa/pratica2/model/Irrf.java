package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "irrf")
public class Irrf {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idirrf;
    @NotNull
    private double valorinicial;
    @NotNull
    private double valorfinal;
    @NotNull
    private float aliquota;
    @NotNull
    private float deducaominima;
    private Date validade;
    private double deducaodependente;

    public int getIdirrf() {
        return this.idirrf;
    }

    public void setIdirrf(int idirrf) {
        this.idirrf = idirrf;
    }

    public double getValorinicial() {
        return this.valorinicial;
    }

    public void setValorinicial(double valorinicial) {
        this.valorinicial = valorinicial;
    }

    public double getValorfinal() {
        return this.valorfinal;
    }

    public void setValorfinal(double valorfinal) {
        this.valorfinal = valorfinal;
    }

    public float getAliquota() {
        return this.aliquota;
    }

    public void setAliquota(float aliquota) {
        this.aliquota = aliquota;
    }

    public float getDeducaominima() {
        return this.deducaominima;
    }

    public void setDeducaominima(float deducaominima) {
        this.deducaominima = deducaominima;
    }

    public Date getValidade() {
        return this.validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    public double getDeducaodependente() {
        return this.deducaodependente;
    }

    public void setDeducaodependente(double deducaodependente) {
        this.deducaodependente = deducaodependente;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Irrf)) {
            return false;
        }
        Irrf irrf = (Irrf) o;
        return idirrf == irrf.idirrf && valorinicial == irrf.valorinicial && valorfinal == irrf.valorfinal
                && aliquota == irrf.aliquota && deducaominima == irrf.deducaominima
                && Objects.equals(validade, irrf.validade) && deducaodependente == irrf.deducaodependente;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idirrf, valorinicial, valorfinal, aliquota, deducaominima, validade, deducaodependente);
    }
}
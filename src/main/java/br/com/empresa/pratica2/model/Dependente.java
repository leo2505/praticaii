package br.com.empresa.pratica2.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.br.CPF;

@Entity
@Table(name = "dependente")
public class Dependente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int iddependente;
    @NotNull
    private String nomedependente;
    @CPF
    private String cpfdependente;
    @NotNull
    private Date datanascimentodependente;
    private boolean depir;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    public int getIddependente() {
        return this.iddependente;
    }

    public void setIddependente(int iddependente) {
        this.iddependente = iddependente;
    }

    public String getNomedependente() {
        return this.nomedependente;
    }

    public void setNomedependente(String nomedependente) {
        this.nomedependente = nomedependente;
    }

    public String getCpfdependente() {
        return this.cpfdependente;
    }

    public void setCpfdependente(String cpfdependente) {
        this.cpfdependente = cpfdependente;
    }

    public Date getDatanascimentodependente() {
        return this.datanascimentodependente;
    }

    public void setDatanascimentodependente(Date datanascimentodependente) {
        this.datanascimentodependente = datanascimentodependente;
    }

    public boolean isDepir() {
        return this.depir;
    }

    public boolean getDepir() {
        return this.depir;
    }

    public void setDepir(boolean depir) {
        this.depir = depir;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Dependente)) {
            return false;
        }
        Dependente dependente = (Dependente) o;
        return iddependente == dependente.iddependente && Objects.equals(nomedependente, dependente.nomedependente)
                && Objects.equals(cpfdependente, dependente.cpfdependente)
                && Objects.equals(datanascimentodependente, dependente.datanascimentodependente)
                && depir == dependente.depir && Objects.equals(idfuncionario, dependente.idfuncionario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iddependente, nomedependente, cpfdependente, datanascimentodependente, depir,
                idfuncionario);
    }
}
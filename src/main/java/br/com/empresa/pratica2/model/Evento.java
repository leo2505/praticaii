package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "evento")
public class Evento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idevento;
    @NotNull
    private String descricaoevento;
    @NotNull
    private String tipoevento;
    private int eventofixo;
    private int incideir;
    private int incideinss;
    private int incidefgts;

    public int getIdevento() {
        return this.idevento;
    }

    public void setIdevento(int idevento) {
        this.idevento = idevento;
    }

    public String getDescricaoevento() {
        return this.descricaoevento;
    }

    public void setDescricaoevento(String descricaoevento) {
        this.descricaoevento = descricaoevento;
    }

    public String getTipoevento() {
        return this.tipoevento;
    }

    public void setTipoevento(String tipoevento) {
        this.tipoevento = tipoevento;
    }

    public int getEventofixo() {
        return this.eventofixo;
    }

    public void setEventofixo(int eventofixo) {
        this.eventofixo = eventofixo;
    }

    public int getIncideir() {
        return this.incideir;
    }

    public void setIncideir(int incideir) {
        this.incideir = incideir;
    }

    public int getIncideinss() {
        return this.incideinss;
    }

    public void setIncideinss(int incideinss) {
        this.incideinss = incideinss;
    }

    public int getIncidefgts() {
        return this.incidefgts;
    }

    public void setIncidefgts(int incidefgts) {
        this.incidefgts = incidefgts;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Evento)) {
            return false;
        }
        Evento evento = (Evento) o;
        return idevento == evento.idevento && Objects.equals(descricaoevento, evento.descricaoevento)
                && Objects.equals(tipoevento, evento.tipoevento) && eventofixo == evento.eventofixo
                && incideir == evento.incideir && incideinss == evento.incideinss && incidefgts == evento.incidefgts;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idevento, descricaoevento, tipoevento, eventofixo, incideir, incideinss, incidefgts);
    }
}
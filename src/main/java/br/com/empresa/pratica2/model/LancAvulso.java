package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lancavulso")
public class LancAvulso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idlancamento;
    private String referencia;
    private float valor;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    @ManyToOne
    @JoinColumn(name = "idevento")
    private Evento idevento;

    public int getIdlancamento() {
        return this.idlancamento;
    }

    public void setIdlancamento(int idlancamento) {
        this.idlancamento = idlancamento;
    }

    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public float getValor() {
        return this.valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Evento getIdevento() {
        return this.idevento;
    }

    public void setIdevento(Evento idevento) {
        this.idevento = idevento;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof LancAvulso)) {
            return false;
        }
        LancAvulso lancAvulso = (LancAvulso) o;
        return idlancamento == lancAvulso.idlancamento && Objects.equals(referencia, lancAvulso.referencia)
                && valor == lancAvulso.valor && Objects.equals(idfuncionario, lancAvulso.idfuncionario)
                && Objects.equals(idevento, lancAvulso.idevento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idlancamento, referencia, valor, idfuncionario, idevento);
    }
}
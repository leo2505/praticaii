package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eventofuncionario")
public class EventoFuncionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ideventofuncionario;
    private int mes;
    private int ano;
    private float valorprovento;
    private float valordesconto;
    private int quantidade;

    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    @ManyToOne
    @JoinColumn(name = "idevento")
    private Evento idevento;

    public int getIdeventofuncionario() {
        return this.ideventofuncionario;
    }

    public void setIdeventofuncionario(int ideventofuncionario) {
        this.ideventofuncionario = ideventofuncionario;
    }

    public int getMes() {
        return this.mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return this.ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public float getValorprovento() {
        return this.valorprovento;
    }

    public void setValorprovento(float valorprovento) {
        this.valorprovento = valorprovento;
    }

    public float getValordesconto() {
        return this.valordesconto;
    }

    public void setValordesconto(float valordesconto) {
        this.valordesconto = valordesconto;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Evento getIdevento() {
        return this.idevento;
    }

    public void setIdevento(Evento idevento) {
        this.idevento = idevento;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof EventoFuncionario)) {
            return false;
        }
        EventoFuncionario eventoFuncionario = (EventoFuncionario) o;
        return ideventofuncionario == eventoFuncionario.ideventofuncionario && mes == eventoFuncionario.mes
                && ano == eventoFuncionario.ano && valorprovento == eventoFuncionario.valorprovento
                && valordesconto == eventoFuncionario.valordesconto && quantidade == eventoFuncionario.quantidade
                && Objects.equals(idfuncionario, eventoFuncionario.idfuncionario)
                && Objects.equals(idevento, eventoFuncionario.idevento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideventofuncionario, mes, ano, valorprovento, valordesconto, quantidade, idfuncionario,
                idevento);
    }
}
package br.com.empresa.pratica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "horariofuncionario")
public class HorarioFuncionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idhorariofuncionario;
    @NotNull
    private int mes;
    @NotNull
    private int ano;
    @NotNull
    private int horastrabalhadas;
    
    @ManyToOne
    @JoinColumn(name = "idfuncionario")
    private Funcionario idfuncionario;

    private int tipohoraextra;
    private int diasuteis;
    private int diasnaouteis;

    public int getIdhorariofuncionario() {
        return this.idhorariofuncionario;
    }

    public void setIdhorariofuncionario(int idhorariofuncionario) {
        this.idhorariofuncionario = idhorariofuncionario;
    }

    public int getMes() {
        return this.mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return this.ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getHorastrabalhadas() {
        return this.horastrabalhadas;
    }

    public void setHorastrabalhadas(int horastrabalhadas) {
        this.horastrabalhadas = horastrabalhadas;
    }

    public Funcionario getIdfuncionario() {
        return this.idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public int getTipohoraextra() {
        return this.tipohoraextra;
    }

    public void setTipohoraextra(int tipohoraextra) {
        this.tipohoraextra = tipohoraextra;
    }

    public int getDiasuteis() {
        return this.diasuteis;
    }

    public void setDiasuteis(int diasuteis) {
        this.diasuteis = diasuteis;
    }

    public int getDiasnaouteis() {
        return this.diasnaouteis;
    }

    public void setDiasnaouteis(int diasnaouteis) {
        this.diasnaouteis = diasnaouteis;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof HorarioFuncionario)) {
            return false;
        }
        HorarioFuncionario horarioFuncionario = (HorarioFuncionario) o;
        return idhorariofuncionario == horarioFuncionario.idhorariofuncionario && mes == horarioFuncionario.mes
                && ano == horarioFuncionario.ano && horastrabalhadas == horarioFuncionario.horastrabalhadas
                && Objects.equals(idfuncionario, horarioFuncionario.idfuncionario)
                && tipohoraextra == horarioFuncionario.tipohoraextra && diasuteis == horarioFuncionario.diasuteis
                && diasnaouteis == horarioFuncionario.diasnaouteis;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhorariofuncionario, mes, ano, horastrabalhadas, idfuncionario, tipohoraextra, diasuteis,
                diasnaouteis);
    }
}